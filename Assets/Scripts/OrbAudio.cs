﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class OrbAudio : MonoBehaviour
{
	private float minVolume = 0f;		// between 0 and 1
	private float maxVolume = 1f;		// between 0 and 1

	private int volumeBeats = 2;		// per audio clock interval

	private int volumeTweenId;

	private Orb lockedOnOrb;

	private AudioSource audioSource;


	//private void Start()
	//{
	//	audioSource = GetComponent<AudioSource>();
	//}

	private void OnEnable()
	{
		GameEvents.OnAudioClock += OnAudioClock;
		GameEvents.OnOrbLockedOn += OnOrbLockedOn;
		GameEvents.OnNoOrbLockedOn += OnNoOrbLockedOn;
	}

	private void OnDisable()
	{
		GameEvents.OnAudioClock -= OnAudioClock;
		GameEvents.OnOrbLockedOn -= OnOrbLockedOn;
		GameEvents.OnNoOrbLockedOn -= OnNoOrbLockedOn;
	}

	private void OnAudioClock(float interval)
	{
		//TweenVolume(interval);
	}
	
	
	private void TweenVolume(float clockInterval, float delay = 0f)
	{
		float tweenTime = clockInterval / (float)volumeBeats;
		
		volumeTweenId = LeanTween.value(gameObject, minVolume, maxVolume, tweenTime)
								.setEase(LeanTweenType.easeInSine)
								.setDelay(delay)
								.setOnUpdate((float val) => { audioSource.volume = val; })
								.setLoopPingPong(volumeBeats).id;
								
		//LTDescr tween = LeanTween.descr(volumeTweenId);
	}
	
	public void SetAudio(AudioClip clip)
	{
		audioSource = GetComponent<AudioSource>();
	
		audioSource.clip = clip;
		StopAudio();
	}

	public void PlayAudio()
	{
		if (! audioSource.isPlaying)
			audioSource.Play();
	}
	
	public void StopAudio()
	{
		if (audioSource.isPlaying)
			audioSource.Stop();
	}
	
	
	private void OnOrbLockedOn(Orb targetOrb)
	{
		if (targetOrb == null)
			return;

		if (lockedOnOrb != null)
			OnNoOrbLockedOn();

		lockedOnOrb = targetOrb;
		lockedOnOrb.orbAudio.PlayAudio();
		lockedOnOrb.orbAudio.transform.position = transform.position;         // so pilot can hear it...
	}
	
	private void OnNoOrbLockedOn()
	{
		if (lockedOnOrb == null)
			return;

		lockedOnOrb.orbAudio.StopAudio();
		lockedOnOrb.orbAudio.transform.localPosition = Vector3.zero;          // return to its owner...
		lockedOnOrb = null;
	}

}
