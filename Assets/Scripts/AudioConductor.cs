﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioConductor : MonoBehaviour
{
	public float clockInterval = 1f;
	public List<AudioClip> orbNotes = new List<AudioClip>();
	
	private bool clockTicking = false;
	private Coroutine clockCoroutine;
	
	
    private void Start()
    {
		ResetClock();
    }

	private void OnEnable()
	{
		GameEvents.OnOrbSpawned += OnOrbSpawned;		// assign random note
	}
	
	private void OnDisable()
	{
		GameEvents.OnOrbSpawned -= OnOrbSpawned;
	}


	private void ResetClock()
	{
		StopClock();
		clockCoroutine = StartCoroutine(AudioClock());
	}

	private IEnumerator AudioClock()
	{
		while (clockTicking)
		{
			GameEvents.OnAudioClock?.Invoke(clockInterval);
			yield return new WaitForSeconds(clockInterval);
		}
	}
	
	private void StopClock()
	{
		clockTicking = false;

		if (clockCoroutine != null)
			StopCoroutine(clockCoroutine);
	}
	
	private void OnOrbSpawned(Orb newOrb)
	{
		var randomIndex = Random.Range(0, orbNotes.Count - 1);
		
		newOrb.orbAudio.SetAudio(orbNotes[randomIndex]);
	}
}