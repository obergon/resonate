﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

/// <summary>
/// player camera, navigation, aiming and 'shooting'
/// </summary>
[RequireComponent(typeof(Camera))]
public class OrbPilot : MonoBehaviour
{
	//public FPVCamera FPVCam;

	public CinemachineVirtualCamera virtualCam;
	
	public float reach;
	public LayerMask interactionLayers;			// orb layer only

	public LineRenderer laserBeam;
	public ParticleSystem laserImpact;
	public AudioSource laserAudio;

	public GameObject shotPrefab;
	public float shotForce;
	public AudioClip crackleAudio;
	public AudioClip shootAudio;
	
	private readonly float shootFromBelow = 0.05f;		// wing tip
	private readonly float shootFromInFront = 1f;		// wing tip
	private readonly float zeroSpeed = 0.01f;			// to determine orb direction
	private readonly float minSpeed = 0.1f;				// don't reduce any further with reverse thrust
	
	public float maxSpeed = 20.0f;						// don't increase any further with forward thrust
	
	public float distanceAbove;
	public float distanceBehind;
	public float smoothTime;

	public float pushPullForce;
	public float leftRightForce = 0.1f;
	public float upDownForce = 0.1f;
	public float attractForce = 0.05f;
	
	private bool unifying = false;
	public float unifyTime = 1.5f;

	public bool thrustOn = false;
	public bool rolling = false;            // rotation under control of pilot

	public float minFuel;					// for thruster to be turned on
	public float maxFuel;
	public float fuelBurnRate;
	public float fuelRefillRate;
	
	private float fuelRemaining;
	private Vector3 collisionPoint;
	private float collisionDistance;

	//private Vector3 yawRotation = Vector3.zero;
	private Vector3 rollRotation = Vector3.zero;
	
	private float rollZAngle;
	public float turnAngle;      		// +/- - increment
	public float maxTurnAngle;    		// +/-
	public float turnSpeed;       		// for rotation lerp
	public float rollReturnTime;

	public Transform wingCentre;        // centre point for wing tip rotation
	public Transform wingTipLeft;       // left wing tip
	public Transform wingTipRight;      // right wing tip

	private Vector3 lookRotation = Vector3.zero;

	private Orb controlledOrb;				// orb being controlled by player
	private float camOrbRadius = 0.5f;
	private float rayCastFactor = 5f;

	private bool orbsSpawned = false;
	private Orb targetOrb;           // from sphere cast (can lock on)

	public enum ControlState
	{
		Forward,			// while not dragging or locked on - following orb
		Searching,			// while dragging - looking around
		//Targeting,			// while dragging, target orb in sights
		LockedOn,			// while searching, if targeting
	}

	private ControlState controlState = ControlState.Forward;

	//private bool IsWatchingOrb { get { return distanceAbove > 0 && distanceBehind > 0; } }
	private Vector3 RayCastPoint { get { return transform.position + (transform.forward * (camOrbRadius * 1.5f)); } }   // in front of camera
																													
	private Rigidbody OrbRigidBody => controlledOrb?.GetComponent<Rigidbody>();
	private float OrbSpeed => controlledOrb != null ? OrbRigidBody.velocity.magnitude : 0f;
	private Vector3 OrbDirection => controlledOrb != null ? OrbRigidBody.velocity.normalized : Vector3.zero;


	private void Start()
	{
		SetControlState(ControlState.Forward, true);
	}

	private void OnEnable()
	{
		GameEvents.OnTouchStart += OnTouchStart;
		GameEvents.OnTouchMove += OnTouchMove;
		GameEvents.OnTouchEnd += OnTouchEnd;
		
		GameEvents.OnMouseScrolled += ChangeOrbAltitude;

		GameEvents.OnOrbsSpawned += OnOrbsSpawned;
		GameEvents.OnPilotOrb += OnPilotOrb;
		GameEvents.OnOrbReflect += OnReflectOrb;
	}

	private void OnDisable()
	{
		GameEvents.OnTouchStart -= OnTouchStart;
		GameEvents.OnTouchMove -= OnTouchMove;
		GameEvents.OnTouchEnd -= OnTouchEnd;
		
		GameEvents.OnMouseScrolled -= ChangeOrbAltitude;

		GameEvents.OnOrbsSpawned -= OnOrbsSpawned;
		GameEvents.OnPilotOrb -= OnPilotOrb;
		GameEvents.OnOrbReflect -= OnReflectOrb;
	}


	private void Update()
	{
		if (Input.GetKey(KeyCode.UpArrow))
		{
			if (targetOrb != null)
				PushTargetOrb();
			else
				OrbForwardThrust();
		}
		else if (Input.GetKey(KeyCode.DownArrow))
		{
			if (targetOrb != null)
				PullTargetOrb();
			else
				OrbReverseThrust();
		}
		
		if (Input.GetKeyUp(KeyCode.UpArrow))
		{
			if (targetOrb == null && thrustOn)
				thrustOn = false;
		}
		else if (Input.GetKeyUp(KeyCode.DownArrow))
		{
			if (targetOrb == null && thrustOn)
				thrustOn = false;
		}


		// roll left / right
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			OrbLeftTurn();
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			OrbRightTurn();
		}
		
		if (Input.GetKeyUp(KeyCode.LeftArrow))
		{
			thrustOn = false;
			ResetToCentre();
		}
		else if (Input.GetKeyUp(KeyCode.RightArrow))
		{
			thrustOn = false;
			ResetToCentre();
		}

		if (Input.GetKeyDown(KeyCode.Space))
			Shoot();

		if (thrustOn)
			fuelRemaining -= fuelBurnRate;
		else
		{
			fuelRemaining += fuelRefillRate;

			if (fuelRemaining > maxFuel)
				fuelRemaining = maxFuel;
		}

		CheckCollisionDistance();
		CheckSpeed();			// limit to maxSpeed
			
		GameEvents.OnPilotStatsUpdate?.Invoke(controlledOrb, OrbSpeed, OrbDirection, lookRotation, thrustOn, fuelRemaining, collisionDistance);
	}


	// always 'follow' controlledOrb
	private void LateUpdate()
 	{
 		if (!orbsSpawned)
			return;

	  	if (controlledOrb == null)
			return;

		// ensure pilot is not positioned exactly at orb position, so we can get direction
		var above = distanceAbove > 0 ? distanceAbove : 0.1f;
		var behind = distanceBehind > 0 ? distanceBehind : 0.1f;

		// lerp to position camera behind and above controlledOrb
		CameraBehindOrb(above, behind);

	    switch (controlState)
		{
			case ControlState.Forward:
				TurnTowards(OrbDirection);		// follow orb
				break;
				
			case ControlState.Searching:
				RayCastForTargetOrb();
				break;
				
			//case ControlState.Targeting:
				//LookAtOrb(targetOrb, true);
				//RayCastForTargetOrb();			// to make sure target still in range
				//break;
				
			case ControlState.LockedOn:
				LookAtOrb(targetOrb, true);
				RayCastForTargetOrb();			// to make sure target still in range
				break;
		}

		//GameEvents.OnOrbStatsUpdate?.Invoke(controlledOrb, OrbSpeed, OrbDirection, lookRotation, thrustOn, fuelRemaining);
    }


	private void Init()
	{
		orbsSpawned = false;
		
		SetControlState(ControlState.Forward, true);
		rollRotation = Vector3.zero;
		
		GameEvents.OnPilotRollRotation?.Invoke(rollRotation.z);

		thrustOn = false;
		rolling = false;
		fuelRemaining = maxFuel;
		
		GameEvents.OnInitFuel?.Invoke(maxFuel, fuelRemaining);
		
		if (controlledOrb != null)
			GameEvents.OnPilotStatsUpdate?.Invoke(controlledOrb, OrbSpeed, OrbDirection, lookRotation, thrustOn, fuelRemaining, collisionDistance);
			
		StartCoroutine(GetComponent<OrbSpawner>().SpawnOrbs(controlledOrb));
	}

	private void SetControlState(ControlState newState, bool force = false)
	{
		if (force || controlState != newState)
		{
			var fromState = controlState;
			controlState = newState;
			
			GameEvents.OnPilotStateChanged?.Invoke(fromState, newState);
			
			if (fromState != ControlState.LockedOn && controlState == ControlState.LockedOn)
				GameEvents.OnOrbLockedOn?.Invoke(targetOrb);        // eg. target audio source moved to pilot location
			else if (fromState == ControlState.LockedOn && controlState != ControlState.LockedOn)
				GameEvents.OnNoOrbLockedOn?.Invoke();
		}
	}


	private void CameraBehindOrb(float above, float behind)
	{
		// lerp to position camera behind and above controlledOrb
		Vector3 targetPosition = controlledOrb.transform.position + (Vector3.up * above) + (OrbDirection * -behind);
		transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * smoothTime);

		GameEvents.OnOrbPilotPosition?.Invoke(controlledOrb, transform.position);
	}


	private void CheckCollisionDistance()
	{
		if (Physics.Raycast(RayCastPoint, transform.forward, out RaycastHit hitInfo))
		{
			collisionPoint = hitInfo.point;
			collisionDistance = (collisionPoint - transform.position).magnitude;
		}
		else
		{
			collisionPoint = transform.position;
			collisionDistance = 0f;
		}
	}

	private void CheckSpeed()
	{
		if (controlledOrb == null)
			return;

		if (OrbSpeed > maxSpeed)
			controlledOrb.LimitSpeed(maxSpeed);
	}


	// touch 'takes control' of camera rotation
	private void OnTouchStart(Vector2 touchStart)
	{
		SetControlState(ControlState.Searching);
	}

	// dragDirection is normalised
	private void OnTouchMove(Vector2 touchPosition, Vector3 dragDirection)
	{
		SetControlState(ControlState.Searching);

		lookRotation = transform.rotation.eulerAngles;
			
		// TODO: don't know why I have to reverse x & y...
		lookRotation.x += dragDirection.y;
		lookRotation.y += dragDirection.x;
		lookRotation.z += dragDirection.z;

		transform.rotation = Quaternion.Euler(lookRotation);
	}

	private void OnTouchEnd(Vector2 touchEnd)
	{
		LaserOff();
		
		if (controlState != ControlState.LockedOn)
		{
			//GameEvents.OnNoOrbLockedOn?.Invoke();
			//Debug.Log("OnNoOrbLockedOn");
			SetControlState(ControlState.Forward);
		}
	}


	private void LookAtOrb(Orb orb, bool direct)		// looks at lookAtPoint if not direct
	{
		if (orb == null)
			return;
	
		var lookAtPoint = orb.transform.position + (Vector3.up * (direct ? 0f : orb.LookAtPoint));
		TurnTowards(lookAtPoint - transform.position);
		
		//Vector3 targetDir = Vector3.RotateTowards(transform.forward, lookAtPoint - transform.position, Time.deltaTime * smoothTime, 0.0f);
		//transform.rotation = Quaternion.LookRotation(targetDir);
		
		//Debug.DrawRay(transform.position, targetDir, Color.cyan);
	}

	private void TurnTowards(Vector3 direction)
	{
		if (rolling)			// pilot rotation under player control
			return;
			
		Vector3 targetDir = Vector3.RotateTowards(transform.forward, direction, Time.deltaTime * smoothTime, 0.0f);
		transform.rotation = Quaternion.LookRotation(targetDir);
	}
	

	private void OrbForwardThrust()
	{
		if (controlledOrb == null)
			return;
			
		if (OrbSpeed > maxSpeed)
		{
			thrustOn = false;
			return;
		}

		if (! CheckFuel())
			return;

		thrustOn = true;
		OrbRigidBody.velocity += (OrbSpeed > zeroSpeed ? OrbDirection : transform.forward) * pushPullForce;
		
		GameEvents.OnOrbForwardThrust?.Invoke(controlledOrb);
	}

	private void OrbReverseThrust()
	{
		if (controlledOrb == null)
			return;

		if (OrbSpeed <= minSpeed)
		{
			thrustOn = false;
			return;
		}
		
		if (! CheckFuel())
			return;
			
		thrustOn = true;
		OrbRigidBody.velocity += (OrbSpeed > zeroSpeed ? OrbDirection : transform.forward) * -pushPullForce;
		
		GameEvents.OnOrbReverseThrust?.Invoke(controlledOrb);
	}

	private void OrbRightTurn()
	{
		if (controlledOrb == null)
			return;
			
		if (! CheckFuel())
			return;
		
		thrustOn = true;
		//rolling = true;

		//OrbRigidBody.velocity += (OrbSpeed > zeroSpeed ? OrbDirection : controlledOrb.transform.forward) * -pushPullForce;
		
		OrbRigidBody.velocity += transform.right * leftRightForce;

		rollRotation = new Vector3(rollRotation.x, rollRotation.y, Mathf.LerpAngle(rollRotation.z, RollZAngle(false), Time.deltaTime * turnSpeed));
		//transform.rotation = Quaternion.Euler(rollRotation);
		GameEvents.OnPilotRollRotation?.Invoke(rollRotation.z);

		//wingCentre.localEulerAngles = new Vector3(0f, 0f, Mathf.LerpAngle(rollRotation.z, RollZAngle(false), Time.deltaTime * turnSpeed));
		wingCentre.localEulerAngles = new Vector3(0f, 0f, rollRotation.z);
		//wingCentre.rotation = Quaternion.Euler(new Vector3(0f, 0f, rollRotation.z));
		//yawRotation = new Vector3(transform.eulerAngles.x, Mathf.LerpAngle(yawRotation.y, YawYAngle(false), Time.deltaTime * turnSpeed), transform.eulerAngles.z);
		//transform.eulerAngles += yawRotation;

		GameEvents.OnOrbTurnRight?.Invoke(controlledOrb);
	}

	private void OrbLeftTurn()
	{
		if (controlledOrb == null)
			return;
			
		if (! CheckFuel())
			return;
		
		thrustOn = true;
		//rolling = true;

		OrbRigidBody.velocity += transform.right * -leftRightForce;
		
		//rollRotation.z = Mathf.LerpAngle(rollRotation.z, RollZAngle(false), Time.deltaTime * turnSpeed);
		rollRotation = new Vector3(rollRotation.x, rollRotation.y, Mathf.LerpAngle(rollRotation.z, RollZAngle(true), Time.deltaTime * turnSpeed));
		//transform.rotation = Quaternion.Euler(rollRotation);

		//wingCentre.localEulerAngles = new Vector3(0f, 0f, Mathf.LerpAngle(rollRotation.z, RollZAngle(true), Time.deltaTime * turnSpeed));
		wingCentre.localEulerAngles = new Vector3(0f, 0f, rollRotation.z);
		//wingCentre.rotation = Quaternion.Euler(new Vector3(0f, 0f, rollRotation.z));
		GameEvents.OnPilotRollRotation?.Invoke(rollRotation.z);
		
		//yawRotation = new Vector3(transform.eulerAngles.x, Mathf.LerpAngle(yawRotation.y, YawYAngle(true), Time.deltaTime * turnSpeed), transform.eulerAngles.z);
		//transform.eulerAngles += yawRotation;
		
		GameEvents.OnOrbTurnLeft?.Invoke(controlledOrb);
	}
	
	private void ChangeOrbAltitude(float delta)
	{
		OrbRigidBody.velocity += delta * (transform.up * upDownForce);
	}
	
	
	private void ResetToCentre()
	{
		if (controlledOrb == null)
			return;
			
		StartCoroutine(ResetRollToCentre());
		//StartCoroutine(ResetYawToCentre());

		GameEvents.OnOrbResetToCentre?.Invoke(controlledOrb);
	}
	
	
	private float RollZAngle(bool leftTurn)
	{
		rollZAngle = rollRotation.z + (leftTurn ? turnAngle : -turnAngle);

		if (rollZAngle > maxTurnAngle)
			rollZAngle = maxTurnAngle;
		else if (rollZAngle < -maxTurnAngle)
			rollZAngle = -maxTurnAngle;

		return rollZAngle;
	}


	private bool CheckFuel()
	{
		return true;		// TODO: reinstate?
		
		//if (fuelRemaining <= 0)
		//{
		//	fuelRemaining = 0f;
		//	thrustOn = false;
		//	return false;
		//}

		//if (!thrustOn && fuelRemaining < minFuel)		// insufficient 'recovery' time
		//{
		//	//thrustOn = false;
		//	return false;
		//}

		//return true;
	}
	
	//private float YawYAngle(bool leftTurn)
	//{
	//	float yawY = yawRotation.y + (leftTurn ? -turnAngle : turnAngle);

	//	if (yawY < 0 && yawY < -maxTurnAngle)
	//		yawY = -maxTurnAngle;
	//	else if (yawY > maxTurnAngle)
	//		yawY = maxTurnAngle;

	//	return yawY;
	//}
	

	private IEnumerator ResetRollToCentre()
	{
		//if (! rolling)
			//yield break;
			
		//rolling = false;		// pilot rotation returns to look at orb
			
		float t = 0;

		while (t < 1.0f)
		{
			rollZAngle = Mathf.LerpAngle(rollRotation.z, 0f, t / rollReturnTime);
			rollRotation = new Vector3(rollRotation.x, rollRotation.y, rollZAngle);
			//transform.rotation = Quaternion.Euler(rollRotation);
			
			GameEvents.OnPilotRollRotation?.Invoke(rollRotation.z);

			wingCentre.localEulerAngles = new Vector3(0f, 0f, rollZAngle);
			
			t += Time.deltaTime;
			yield return null;
		}

		rollZAngle = 0f;
		rollRotation = Vector3.zero;
		
		GameEvents.OnPilotRollRotation?.Invoke(rollRotation.z);

		rolling = false;		// pilot rotation returns to look at orb
		yield return null;
	}
	
	//private IEnumerator ResetYawToCentre()
	//{
	//	float t = 0;

	//	while (t < 1.0f)
	//	{
	//		yawRotation = new Vector3(0f, Mathf.LerpAngle(yawRotation.y, 0f, t / turnSpeed), 0f);
	//		transform.eulerAngles = yawRotation;
			
	//		t += Time.deltaTime;
	//		yield return null;
	//	}
	
	//	yawRotation = Vector3.zero;
	//	yield return null;
	//}
	

	// attract to each other
	private void PullTargetOrb()
	{
		if (targetOrb == null)
			return;

		if (unifying)
			return;

		unifying = true;
		targetOrb.GetComponent<Rigidbody>().velocity = Vector3.zero;
		OrbRigidBody.velocity = Vector3.zero;

		LeanTween.move(targetOrb.gameObject, controlledOrb.transform, unifyTime)
					.setEase(LeanTweenType.easeInExpo)
					.setOnComplete(() => { unifying = false; });

		//Vector3 direction = targetOrb.transform.position - controlledOrb.transform.position;
		//targetOrb.GetComponent<Rigidbody>().velocity += direction * -attractForce;
		//OrbRigidBody.velocity += direction * attractForce;
	}

	// repel away from each other
	private void PushTargetOrb()
	{
		if (targetOrb == null)
			return;
			
		OrbRigidBody.velocity = Vector3.zero;

		Vector3 direction = targetOrb.transform.position - controlledOrb.transform.position;
		targetOrb.GetComponent<Rigidbody>().velocity += direction * attractForce;
		//OrbRigidBody.velocity += direction * -attractForce;
	}


	private void RayCastForTargetOrb()
	{
		var raycastHits = Physics.SphereCastAll(RayCastPoint, camOrbRadius * rayCastFactor, transform.forward, reach, interactionLayers);

		foreach (var hit in raycastHits)
		{
			targetOrb = hit.collider.GetComponent<Orb>();
			
			if (targetOrb.GetInstanceID() != controlledOrb.GetInstanceID())
			{
				//Debug.Log("RayCastForTargetOrb - targetOrb: " + targetOrb.GetInstanceID() + ", controlledOrb: " + controlledOrb.GetInstanceID());

				if (controlState == ControlState.Searching)
					SetControlState(ControlState.LockedOn);

				//if (controlState == ControlState.LockedOn)
				//{
				//	//Debug.Log("OnOrbLockedOn: " + targetOrb.name);
				//	GameEvents.OnOrbLockedOn?.Invoke(targetOrb);        // eg. target audio source moved to pilot location
				//}
					
				//LaserTargetOrb(targetOrb);
				return;
			}
		}

		// no target orb 'seen'
		if (controlState == ControlState.LockedOn)      // no longer locked on
			SetControlState(ControlState.Forward);
			
		//if (targetOrb != null)      // no longer locked on
		//{
		//	GameEvents.OnNoOrbLockedOn?.Invoke();
		//	//Debug.Log("OnNoOrbLockedOn");
		//}
			
		targetOrb = null;
		LaserOff();
	}

	
	private void LaserTargetOrb(Orb target)
	{
		if (target == null)
			return;

		Orb laserTargetOrb = null;

		if (Physics.Raycast(RayCastPoint, transform.forward, out RaycastHit hitInfo, reach, interactionLayers))
		{
			laserTargetOrb = hitInfo.collider.GetComponent<Orb>();

			if (laserTargetOrb.GetInstanceID() == target.GetInstanceID())
			{
				laserBeam.SetPosition(0, ShootFromPoint(transform));		// laser from camera (towards crosshair)
				laserBeam.SetPosition(1, hitInfo.point);

				GameEvents.OnOrbCamLaserTarget?.Invoke(target, hitInfo.point, hitInfo.distance);

				laserImpact.transform.position = hitInfo.point;
				if (! laserImpact.isPlaying)
					laserImpact.Play();

				if (laserAudio != null && !laserAudio.isPlaying)
					laserAudio.Play();
			}
			else
				laserTargetOrb = null;
		}

		if (laserTargetOrb == null)
		{
			GameEvents.OnOrbCamNoLaserTarget?.Invoke();
			LaserOff();
		}
	}


	private void LaserOff()
	{
		laserBeam.SetPosition(0, transform.position);
		laserBeam.SetPosition(1, transform.position);

		laserImpact.Stop();
		
		if (laserAudio != null && laserAudio.isPlaying)
			laserAudio.Stop();
	}
	
	
	private Vector3 ShootFromPoint(Transform shootFrom)
	{
		return shootFrom.position + (Vector3.up * -shootFromBelow) + (transform.forward * shootFromInFront);
	}
	
	
	// fire from both wing tips
	private void Shoot()
	{
		LaserOff();

		if (shotPrefab == null)
			return;

		// raycast will hit torus if nothing else...
		//if (Physics.Raycast(RayCastPoint, transform.forward, out RaycastHit hitInfo))
		if (collisionDistance > 0 && collisionPoint != transform.position)		// TODO: shoot range?
		{
			//laserImpact.transform.position = hitInfo.point;
			//if (!laserImpact.isPlaying)
			//laserImpact.Play();
			
			var leftShootPoint = ShootFromPoint(wingTipLeft);	
			var rightShootPoint = ShootFromPoint(wingTipRight);	
			var leftShootDirection = (collisionPoint - leftShootPoint).normalized;
			var rightShootDirection = (collisionPoint - rightShootPoint).normalized;
			var leftShot = Instantiate(shotPrefab, leftShootPoint, Quaternion.identity) as GameObject;
			var rightShot = Instantiate(shotPrefab, rightShootPoint, Quaternion.identity) as GameObject;

			leftShot.GetComponent<Rigidbody>().velocity = leftShootDirection * shotForce;
			rightShot.GetComponent<Rigidbody>().velocity = rightShootDirection * shotForce;
			
			if (crackleAudio != null)
				AudioSource.PlayClipAtPoint(crackleAudio, Vector3.zero);
			if (shootAudio != null)
				AudioSource.PlayClipAtPoint(shootAudio, Vector3.zero);
		}
	}


	private void OnOrbsSpawned()
	{
		orbsSpawned = true;
	}


	// called whn player takes control of an orb (to pilot it)
	private void OnPilotOrb(Orb orb)
	{
		if (controlledOrb == orb)
			return;
			
		if (controlledOrb != null)
			controlledOrb.isPlayer = false;
			
		controlledOrb = orb;
		controlledOrb.isPlayer = true;
		camOrbRadius = controlledOrb.GetComponent<SphereCollider>().radius;

		//controlledOrb.orbAudio.PlayAudio();

		//FPVCam.followOrb = orb;

		virtualCam.Follow = orb.transform;
		virtualCam.LookAt = orb.transform;
		
		//StartCoroutine(GetComponent<OrbSpawner>().SpawnOrbs(controlledOrb));

		Init();
	}
	
	
	private void OnReflectOrb(Orb orb)
	{
		if (orb.GetInstanceID() != controlledOrb.GetInstanceID())
			return;

		ResetToCentre();
	}
	

    private void OnDrawGizmos()
    {
  //      Gizmos.color = Color.yellow;
		//Gizmos.DrawWireSphere(RayCastPoint, camOrbRadius * rayCastFactor); // + transform.forward * (camOrbRadius * 2f), camOrbRadius);
        //Gizmos.DrawWireSphere(transform.position + transform.forward * reach, camOrbRadius * rayCastFactor);
    }
}
