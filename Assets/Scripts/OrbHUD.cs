﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrbHUD : MonoBehaviour
{
	public Image targetCrossHair;
	public Image rotationCrossHair;
	public float rotationReturnTime = 4;

	public Color crossHairForward;
	public Color crossHairSearching;
	public Color crossHairTarget;
	public Color crossHairLocked;

	public Text ControlState;       // debug only
	public Text ThrusterOn;       // debug only
	
	public Text OrbSpeed; 
	public Text OrbDirection;
	public Text OrbAltitude;
	public Text LookDirection;
	public Text CollisionDistance;
	
	public Text RollRotation;
	public Text FuelRemaining;

	public Slider FuelGauge;

	private Vector3 crossHairAngle = Vector3.zero;
	public float turnAngle = 10f;       // +/-
	public float maxTurnAngle = 60f;    // +/-
	public float turnSpeed = 2f;        // for rotation lerp

	private void Start()
	{
		crossHairAngle = rotationCrossHair.transform.localEulerAngles;
	}


	private void OnEnable()
	{
		//GameEvents.OnOrbTurnLeft += OrbLeftTurn;
		//GameEvents.OnOrbTurnRight += OrbRightTurn;

		GameEvents.OnPilotStateChanged += OnPilotStateChanged;
		GameEvents.OnPilotRollRotation += OnPilotRollRotation;
		GameEvents.OnPilotStatsUpdate += OnOrbStatsUpdate;
		GameEvents.OnInitFuel += OnInitFuel;
	}

	private void OnDisable()
	{
		//GameEvents.OnOrbTurnLeft -= OrbLeftTurn;
		//GameEvents.OnOrbTurnRight -= OrbRightTurn;

		GameEvents.OnPilotStateChanged -= OnPilotStateChanged;
		GameEvents.OnPilotRollRotation -= OnPilotRollRotation;
		GameEvents.OnPilotStatsUpdate -= OnOrbStatsUpdate;
		
		GameEvents.OnInitFuel -= OnInitFuel;
	}

	// called every frame turn key is pressed
	//private void OrbRightTurn(Orb orb)
	//{
	//	crossHairAngle = new Vector3(0f, 0f, Mathf.LerpAngle(crossHairAngle.z, CrossHairZAngle(false), Time.deltaTime * turnSpeed));
	//	rotationCrossHair.transform.localEulerAngles = crossHairAngle;
	//}

	//// called every frame turn key is pressed
	//private void OrbLeftTurn(Orb orb)
	//{
	//	crossHairAngle = new Vector3(0f, 0f, Mathf.LerpAngle(crossHairAngle.z, CrossHairZAngle(true), Time.deltaTime * turnSpeed));
	//	rotationCrossHair.transform.localEulerAngles = crossHairAngle;
	//}
	
	private void OnPilotStateChanged(OrbPilot.ControlState fromState, OrbPilot.ControlState newState)
	{
		ControlState.text = newState.ToString();

		switch (newState)
		{
			case OrbPilot.ControlState.Forward:
				targetCrossHair.color = crossHairForward;
				break;

			case OrbPilot.ControlState.Searching:
				targetCrossHair.color = crossHairSearching;
				break;
								
			//case OrbPilot.ControlState.Targeting:
				//targetCrossHair.color = crossHairTarget;
				//break;

			case OrbPilot.ControlState.LockedOn:
				targetCrossHair.color = crossHairLocked;
				break;
		}
	}
	
	private void OnOrbStatsUpdate(Orb controlledOrb, float orbSpeed, Vector3 orbDirection, Vector3 lookDirection, bool thrustOn, float fuelRemaining, float collisionDistance)
	{
		OrbSpeed.text = "SPEED: " + ((int)Mathf.Ceil(orbSpeed * 10f)).ToString();				// round up
		OrbDirection.text = "DIRECTION: " + orbDirection.ToString();
		OrbAltitude.text = "ALTITUDE: " + ((int)Mathf.Ceil(orbDirection.y * 100f)).ToString();
		LookDirection.text = "FACING: " + (int)lookDirection.x + ", " + (int)lookDirection.y;
		FuelRemaining.text = "FUEL: " + ((int)fuelRemaining).ToString();
		CollisionDistance.text = "COLLISION: " + ((int)collisionDistance).ToString();

		FuelGauge.value = fuelRemaining;

		if (thrustOn)
			ThrusterOn.text = "THRUSTER ON";
		else
			ThrusterOn.text = "THRUSTER OFF";
	}
	
	private void OnPilotRollRotation(float rollRotation)
	{
		RollRotation.text = "ROLL: " + ((int)Mathf.Ceil(rollRotation)).ToString() + " DEG";		// round up
		
		crossHairAngle = new Vector3(0f, 0f, rollRotation);
		rotationCrossHair.transform.localEulerAngles = crossHairAngle;
	}
	
	
	private void OnInitFuel(float maxFuel, float fuelRemaining)
	{
		FuelGauge.maxValue = maxFuel;
		FuelGauge.value = fuelRemaining;
	}
}
