﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbSpawner : MonoBehaviour
{
	public GameObject OrbPrefab;
	public Transform OrbPool;		// folder

	public float MinVelocity;	  	// for random launch speed
	public float MaxVelocity;	 	// for random launch speed
	//public float MinPosition;	 	// for random launch direction
	public float MaxPosition;       // for random launch direction

	public float spawnPause;		// to avoid collisions with spawned orbs

	public int SpawnQuantity;

	public Material PilotMaterial;
	public List<Material> OrbMaterials = new List<Material>();
	
	public List<Orb> OrbList = new List<Orb>();


	private void OnEnable()
	{
		GameEvents.OnPilotOrb += OnPilotOrb;
	}

	private void OnDisable()
	{
		GameEvents.OnPilotOrb -= OnPilotOrb;
	}


	public IEnumerator SpawnOrbs(Orb controlledOrb)
	{
		for (int i = 0; i < SpawnQuantity; i++)
		{
			var newOrb = Instantiate(OrbPrefab, controlledOrb.transform.position, Quaternion.identity, OrbPool);
			newOrb.GetComponent<Collider>().enabled = false;  // to prevent launch collisions

			newOrb.GetComponent<Renderer>().material = RandomOrbMaterial();
			
			OrbList.Add(newOrb.GetComponent<Orb>());
			
			GameEvents.OnOrbSpawned?.Invoke(newOrb.GetComponent<Orb>());
		}
		
		yield return new WaitForSeconds(spawnPause);

		foreach (var orb in OrbList)
		{
			float spawnSpeed = UnityEngine.Random.Range(MinVelocity, MaxVelocity);
			float spawnX = UnityEngine.Random.Range(-MaxPosition, MaxPosition);
			float spawnY = UnityEngine.Random.Range(-MaxPosition, MaxPosition);
			float spawnZ = UnityEngine.Random.Range(-MaxPosition, MaxPosition);
			
			// launch
			orb.GetComponent<Rigidbody>().velocity = new Vector3(spawnX, spawnY, spawnZ).normalized * spawnSpeed;
		}

		yield return new WaitForSeconds(spawnPause);

		foreach (var orb in OrbList)
		{
			orb.GetComponent<Collider>().enabled = true;
		}

		GameEvents.OnOrbsSpawned?.Invoke();
	}

	private Material RandomOrbMaterial()
	{
		int randomIndex = UnityEngine.Random.Range(0, OrbMaterials.Count - 1);
		return OrbMaterials[randomIndex];
	}
	
	private void OnPilotOrb(Orb orb)
	{
		if (orb != null)
			orb.GetComponent<Renderer>().material = RandomOrbMaterial();
			
		orb.GetComponent<Renderer>().material = PilotMaterial;
	}
}
