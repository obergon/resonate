﻿using System;
using UnityEngine;

public static class GameEvents
{
	public delegate void OnTouchStartDelegate(Vector2 touchStart);
	public static OnTouchStartDelegate OnTouchStart;
	
	public delegate void OnTouchMoveDelegate(Vector2 position, Vector3 lookRotation);
	public static OnTouchMoveDelegate OnTouchMove;
	
	public delegate void OnTouchEndDelegate(Vector2 touchStart);
	public static OnTouchEndDelegate OnTouchEnd;
	
	public delegate void OnMouseScrolledDelegate(float delta);
	public static OnMouseScrolledDelegate OnMouseScrolled;
	
	
	public delegate void OnControllerStateChangedDelegate(OrbPilot.ControlState fromState, OrbPilot.ControlState newState);
	public static OnControllerStateChangedDelegate OnPilotStateChanged;
	
	
	public delegate void OnOrbSpawnedDelegate(Orb orb);
	public static OnOrbSpawnedDelegate OnOrbSpawned;
	
	public delegate void OnOrbsSpawnedDelegate();
	public static OnOrbsSpawnedDelegate OnOrbsSpawned;				// random directions, speeds, colours, sounds
	
	public delegate void OnControlOrbDelegate(Orb orb);
	public static OnControlOrbDelegate OnPilotOrb;					// when OrbPilot takes control of an Orb
	
	public delegate void OnOrbPilotPositionDelegate(Orb controlledOrb, Vector3 pilotPosition);
	public static OnOrbPilotPositionDelegate OnOrbPilotPosition;
	
	public delegate void OnOrbForwardThrustDelegate(Orb orb);
	public static OnOrbForwardThrustDelegate OnOrbForwardThrust;				// not currently used
	
	public delegate void OnOrbReverseThrustDelegate(Orb orb);
	public static OnOrbReverseThrustDelegate OnOrbReverseThrust;				// not currently used
	
	public delegate void OnOrbThrustLeftDelegate(Orb orb);
	public static OnOrbThrustLeftDelegate OnOrbTurnLeft;						// not currently used
	
	public delegate void OnOrbThrustRightDelegate(Orb orb);
	public static OnOrbThrustRightDelegate OnOrbTurnRight;						// not currently used
	
	public delegate void OnOrbReturnToCentreDelegate(Orb orb);
	public static OnOrbReturnToCentreDelegate OnOrbResetToCentre;				// not currently used
	
	public delegate void OnOrbReflectDelegate(Orb orb);
	public static OnOrbReflectDelegate OnOrbReflect;	
	
	public delegate void OnPilotStatsUpdateDelegate(Orb controlledOrb, float orbSpeed, Vector3 orbDirection, Vector3 lookDirection, bool thrustOn, float fuelRemaining, float collisionDistance);
	public static OnPilotStatsUpdateDelegate OnPilotStatsUpdate;
	
	public delegate void OnInitFuelDelegate(float maxFuel, float fuelRemaining);
	public static OnInitFuelDelegate OnInitFuel;
	
	public delegate void OnPilotRollRotationDelegate(float rollRotation);
	public static OnPilotRollRotationDelegate OnPilotRollRotation;
	
	
	public delegate void OnOrbLockedOnDelegate(Orb targetOrb);
	public static OnOrbLockedOnDelegate OnOrbLockedOn;	
	
	public delegate void OnNoOrbLockedOnDelegate();
	public static OnNoOrbLockedOnDelegate OnNoOrbLockedOn;		
	
	
	public delegate void OnOrbCamLaserTargetDelegate(Orb orb, Vector3 hotPoint, float hitDistance);
	public static OnOrbCamLaserTargetDelegate OnOrbCamLaserTarget;				// not currently used
	
	public delegate void OnOrbCamNoLaserTargetDelegate();
	public static OnOrbCamNoLaserTargetDelegate OnOrbCamNoLaserTarget;			// not currently used
	
	
	public delegate void OnAudioClockDelegate(float interval);
	public static OnAudioClockDelegate OnAudioClock;
}

