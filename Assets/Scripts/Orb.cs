﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour
{
	private Vector3 lastFrameVelocity;          // to maintain constant speed
	private Rigidbody rb;
	
	public bool isPlayer;
	public float LookAtPoint;       // for cameras to look above / below

	public OrbAudio orbAudio;

	public Transform unifiedOrbs;       // container (parent) for unified orbs

	public List<Orb> orbsUnified = new List<Orb>();
	
	
	private void Start()
    {
    	if (isPlayer)
			GameEvents.OnPilotOrb?.Invoke(this);

		rb = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		lastFrameVelocity = rb.velocity;
	}


	private void OnCollisionEnter(Collision collision)
	{
		//Debug.Log("Orb.OnCollisionEnter: " + collision.gameObject.name);

		if (collision.gameObject.CompareTag("orb"))
		{
			var otherOrb = collision.gameObject.GetComponent<Orb>();

			// consolidate (unite) with player orb
			if (otherOrb != null && otherOrb.isPlayer)
			{
				otherOrb.UniteWithOrb(this);
				return;
			}
		}

		Reflect(collision);
	}

	public void UniteWithOrb(Orb orb)
	{
		orb.GetComponent<Rigidbody>().velocity = Vector3.zero;
			
		orb.orbAudio.transform.localPosition = Vector3.zero;
		orb.orbAudio.PlayAudio();

		orb.GetComponent<Collider>().enabled = false;

		//orb.transform.position = transform.position;
		orb.transform.parent = unifiedOrbs;
		orb.transform.localPosition = Vector3.zero;
		
		orbsUnified.Add(orb);
	}
	
	private void Reflect(Collision collision)
	{
		Vector3 collisionNormal = collision.contacts[0].normal;
		
		//Debug.Log("Reflect: " + collisionNormal);

		var lastFrameSpeed = lastFrameVelocity.magnitude;
		var lastFrameDirection = lastFrameVelocity.normalized;

		var newDirection = Vector3.Reflect(lastFrameDirection, collisionNormal);
		var desiredVelocity = newDirection * lastFrameSpeed;

		rb.velocity = desiredVelocity;      // reflect at same speed exactly
		
		GameEvents.OnOrbReflect?.Invoke(this);
	}

	public void LimitSpeed(float maxSpeed)
	{
		rb.velocity = rb.velocity.normalized * maxSpeed;
	}
}
