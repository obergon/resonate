﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPVCamera : MonoBehaviour
{
	public float distanceBehind;
	public float distanceAbove;
	public float smoothTime;

	public Orb followOrb;

	private Vector3 targetPosition;


	//private void LateUpdate()
	//{
	//	if (followOrb == null)
	//		return;
			
	//	targetPosition = followOrb.transform.position + (Vector3.up * distanceAbove) - (followOrb.transform.forward * distanceBehind);

	//	//Debug.DrawRay(followOrb.transform.position, Vector3.up * distanceAbove, Color.red);
	//	//Debug.DrawRay(followOrb.transform.position, followOrb.transform.forward * distanceBehind, Color.blue);
	//	//Debug.DrawLine(followOrb.transform.position, targetPosition, Color.magenta);

	//	transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * smoothTime);
		
	//	LookAtOrb();
	//}
	
	//private void LookAtOrb()
	//{
	//	var lookAtPoint = followOrb.transform.position + (Vector3.up * followOrb.LookAtPoint);
	//	Vector3 targetDir = Vector3.RotateTowards(transform.forward, lookAtPoint - transform.position, Time.deltaTime * smoothTime, 0.0f);
	//	transform.rotation = Quaternion.LookRotation(targetDir);
		
	//	//Debug.DrawRay(transform.position, targetDir, Color.green);
	//}
}
