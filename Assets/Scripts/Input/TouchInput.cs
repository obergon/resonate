﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TouchInput : MonoBehaviour
{
	private Vector3 touchStart = Vector3.zero;
	private Vector3 lastTouchPosition = Vector3.zero;
	private Vector3 touchEnd = Vector3.zero;

	private Vector3 dragDirection = Vector3.zero;

	private float dragFactor = 0.5f;		// to slow down drag effect

	void Update()
	{
		// track a single touch as a direction control
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			HandleTouch(touch.fingerId, touch.position, touch.phase);
		}
		// Simulate touch events from left mouse button events
		else if (Input.touchCount == 0)
		{
			if (Input.GetMouseButtonDown(0))
				HandleTouch(10, Input.mousePosition, TouchPhase.Began);

			else if (Input.GetMouseButton(0))
				HandleTouch(10, Input.mousePosition, TouchPhase.Moved);

			else if (Input.GetMouseButtonUp(0))
				HandleTouch(10, Input.mousePosition, TouchPhase.Ended);
		}
		
		if (Input.mouseScrollDelta.y < 0 || Input.mouseScrollDelta.y > 0)
			MouseScrolled(Input.mouseScrollDelta.y);
	}
	
	private void HandleTouch(int touchFingerId, Vector3 touchPosition, TouchPhase touchPhase)
	{
		switch (touchPhase)
		{
			case TouchPhase.Began:
				touchStart = lastTouchPosition = touchPosition;
				GameEvents.OnTouchStart?.Invoke(touchStart);
				//Debug.Log("Began: touchStart = " + touchStart);
				break;

			case TouchPhase.Moved:
				if (lastTouchPosition != Vector3.zero && touchPosition != lastTouchPosition)
				{
					dragDirection = (touchPosition - lastTouchPosition) * dragFactor; //.normalized;
					GameEvents.OnTouchMove?.Invoke(touchPosition, dragDirection);
					//Debug.Log("Moved: dragDirection = " + dragDirection + ", lastTouchPosition = " + lastTouchPosition + ", touchPosition = " + touchPosition);
				}
				lastTouchPosition = touchPosition;
				break;

			case TouchPhase.Ended:
				touchEnd = touchPosition;
				touchPosition = Vector3.zero;
				lastTouchPosition = Vector3.zero;
				//Debug.Log("Began: touchEnd = " + touchPosition);
				GameEvents.OnTouchEnd?.Invoke(touchEnd);
				break;

			case TouchPhase.Stationary:
				break;

			case TouchPhase.Canceled:
				break;
		}
	}

	private void MouseScrolled(float delta)
	{
		GameEvents.OnMouseScrolled?.Invoke(delta);
	}
}
