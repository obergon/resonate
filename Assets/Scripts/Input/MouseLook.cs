﻿
using UnityEngine;
using System.Collections;
 
/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation
 
/// To make an FPS style character:
/// - Create a capsule.
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSInputController script to the capsule
///   -> A CharacterMotor and a CharacterController component will be automatically added.
 
/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)
[AddComponentMenu("Camera-Control/Mouse Look")]
public class MouseLook : MonoBehaviour
{
	//public float speed = 3;
	
	//public float minimumX = -360F;
 //   public float maximumX = 360F;
 
 //   public float minimumY = -60F;
 //   public float maximumY = 60F;

	//private Vector3 lookRotation = new Vector3(0, 90, 90);

	//void Update()
	//{
	//	if (Input.GetMouseButton(0))
	//	{
	//		lookRotation.y += Input.GetAxis("Mouse Y");		// up/down
	//		//lookRotation.y = Mathf.Clamp(rotation.y, minimumY, maximumY);
			
	//		lookRotation.x += Input.GetAxis("Mouse X");	// left/right
	//		//lookRotation.x = Mathf.Clamp(rotation.x, minimumX, maximumX);
			
	//		//Debug.Log("rotation.x (L/R) = " + lookRotation.x + ", rotation.y (U/D) = " + lookRotation.y);
			
	//		transform.eulerAngles = lookRotation * speed;
	//	}
	//}
}