﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilotTracker : MonoBehaviour
{
	public float distanceAbove = 900;
	
	private void OnEnable()
	{
		GameEvents.OnOrbPilotPosition += OnOrbPilotPosition;
	}
	
	private void OnDisable()
	{
		GameEvents.OnOrbPilotPosition -= OnOrbPilotPosition;
	}

 	private void OnOrbPilotPosition(Orb controlledOrb, Vector3 pilotPosition)
	{
		var orbPosition = controlledOrb.transform.position;
		transform.position = new Vector3(orbPosition.x, orbPosition.y + distanceAbove, orbPosition.z);
	}

}
