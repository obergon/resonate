﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]

public class PerfectRebound : MonoBehaviour
{
	public float horizontalTolerance;           // considered horizontal if rebound angle very closet to inward angle
	public float horizontalTilt;                // degrees to rotate to break stuck in horizontal pattern

	private Vector3 lastFrameVelocity;          // to maintain constant speed
	private Rigidbody rb;


	//static readonly Vector3[] Directions45 = {
	//	Vector3.up,
	//	Vector3.down,
	//	Vector3.left,
	//	Vector3.right,
	//	(Vector3.up + Vector3.right).normalized,
	//	(Vector3.up + Vector3.left).normalized,
	//	(Vector3.down + Vector3.right).normalized,
	//	(Vector3.down + Vector3.left).normalized,
	//};
	
	static readonly Vector3[] Directions90 = {
		Vector3.up,
		Vector3.down,
		Vector3.left,
		Vector3.right,
	};


	private void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		lastFrameVelocity = rb.velocity;
	}

	private void OnTriggerEnter(Collider other)
	{
		Debug.Log("OnTriggerEnter: " + other.name);
	}

	public void Reflect(Collider other)
	{
		//Vector3 collisionNormal = ClampTo90(collision.contacts[0].normal);
		Vector3 collisionNormal = other.transform.position;
		
		Debug.Log("Reflect: " + collisionNormal);

		var lastFrameSpeed = lastFrameVelocity.magnitude;
		var lastFrameNormal = lastFrameVelocity.normalized;
		
		var newDirection = Vector3.Reflect(lastFrameNormal, collisionNormal);
		//Debug.DrawRay(collision.contacts[0].point, collisionNormal, Color.cyan, 1.5f);
		//Debug.Log(collision.contacts.Length);

		// check for near-zero y-axis rebound and rotate slightly
		var desiredVelocity = newDirection * lastFrameSpeed;

		//if (Mathf.Abs(desiredVelocity.y) < horizontalTolerance)
		//{
		//	// rotate downwards by a small amount
		//	desiredVelocity = Quaternion.Euler(0, 0, horizontalTilt) * desiredVelocity;
		//	//Debug.Log("Reflect horizontal: tilted to " + horizontalTilt);
		//}

		rb.velocity = desiredVelocity;      // reflect at same speed exactly
	}


	/// <summary>
	/// Clamps the given collision normal to the nearest 90 deg angle by finding the
	/// closest normal
	/// </summary>
	/// <returns>The to45.</returns>
	/// <param name="collisionNormal">V.</param>
	private Vector3 ClampTo90(Vector3 collisionNormal)
	{
		Vector3 result = Directions90[0];
		var normal = collisionNormal.normalized;
		float nearestNormal = Vector3.Dot(normal, Directions90[0]);      // .Dot returns 1 to -1 to indicate closeness to vertical (0 is horizontal)

		for (int i = 1; i < Directions90.Length; i++)
		{
			var dotToCompare = Vector3.Dot(normal, Directions90[i]);

			if (dotToCompare > nearestNormal)
			{
				nearestNormal = dotToCompare;
				result = Directions90[i];
			}
		}

		return result;
	}
}
